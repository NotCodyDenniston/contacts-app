package com.example.contactsapp.view.screens

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.example.contactsapp.R
import com.example.contactsapp.model.local.Address
import com.example.contactsapp.model.local.Contact
import com.example.contactsapp.ui.theme.ContactsAppTheme
import com.example.contactsapp.utils.Resource
import com.example.contactsapp.viewmodel.ContactState
import com.example.contactsapp.viewmodel.MainViewModel

class CreateScreen : Fragment() {
    val mainViewModel: MainViewModel by activityViewModels()
    @SuppressLint("SuspiciousIndentation")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                ContactsAppTheme {
                            ShowCreateItems(
                                saveContact = {con->mainViewModel.saveContact(con)},
                                navigate = { findNavController().navigateUp() })
                    }
                }
            }
        }
    }


@Composable
fun ShowCreateItems( saveContact: (Contact)->Unit, navigate: () -> Unit) {

    val _contact = remember {
        mutableStateOf(Contact())
    }
    val _address = remember {
        mutableStateOf(Address())
    }
    val emailCount = remember {
        mutableStateOf(0)
    }
    val phoneCount = remember {
        mutableStateOf(0)
    }
    Column(
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.padding(top = 20.dp)
    ) {
        TextFieldGroups(value = _contact.value.firstName, name = "First Name -Required") {
            _contact.value = _contact.value.copy(firstName = it)
        }
        TextFieldGroups(value = _contact.value.lastName, name = "Last Name -Required") {
            _contact.value = _contact.value.copy(lastName = it)
        }
        TextFieldGroups(value = _contact.value.singleEmail, name = "email") {
            _contact.value = _contact.value.copy(singleEmail = it)
        }
        Row(verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.Center) {
        Button(onClick = { emailCount.value = emailCount.value+1
            _contact.value = _contact.value.copy(email =
            _contact.value.email + _contact.value.singleEmail)
        }) {
            Text(text = "Add email")
        }
        Text(text = "${emailCount.value} emails added")
        }
        TextFieldGroups(value = _contact.value.singlePhone, name = "phone") {
            _contact.value = _contact.value.copy(singlePhone = it)
        }
        Row(verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.Center) {
        Button(onClick = { phoneCount.value = phoneCount.value+1
            _contact.value = _contact.value.copy(phone =
            _contact.value.phone + _contact.value.singlePhone)
        }) {
            Text(text = "Add phone")
        }
        Text(text = "${phoneCount.value} phone numbers added")
        }

        _contact.value.address?.let {
            TextFieldGroups(value = it.streetAddress, name = "Street Address") {
                _address.value = _address.value.copy(streetAddress = it)
                _contact.value = _contact.value.copy(address = _address.value)
            }
        }
       _contact.value.address?.let {
            TextFieldGroups(value = it.city, name = "City") {
                _address.value = _address.value.copy(city = it)
                _contact.value = _contact.value.copy(address = _address.value)
            }
        }
        _contact.value.address?.let {
            TextFieldGroups(value = it.state, name = "State") {
                _address.value = _address.value.copy(state = it)
                _contact.value = _contact.value.copy(address = _address.value)
            }
        }
        _contact.value.address?.let {
            TextFieldGroups(value = it.zipcode, name = "Zipcode") {
                _address.value = _address.value.copy(zipcode = it)
                _contact.value = _contact.value.copy(address = _address.value)
            }
        }
//        TextFieldGroups(value = city)
//        TextFieldGroups(value = state)
//        TextFieldGroups(value = zipcode)
        Button(onClick = {
            if (_contact.value.firstName.isNotEmpty() && _contact.value.lastName.isNotEmpty()){
            saveContact(_contact.value)
            navigate()
            } else {

            }
        }) {
            Text(text = "Save Contact")
        }
        Button(onClick = { navigate() }) {
            Text(text = "Go Back")
        }
    }
}
@Composable
fun TextFieldGroups(value: String, name:String, onTextChange: (newText: String) -> Unit) {
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(text = name)
        BasicTextField(
            value = value,
            onValueChange = { onTextChange(it) },
            Modifier.background(color = Color.LightGray)
        )
    }
}
