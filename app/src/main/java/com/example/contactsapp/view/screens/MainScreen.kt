package com.example.contactsapp.view.screens

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.layout
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.contactsapp.R
import com.example.contactsapp.model.local.Contact
import com.example.contactsapp.ui.theme.ContactsAppTheme
import com.example.contactsapp.viewmodel.MainViewModel

class MainScreen : Fragment() {
    val mainViewModel: MainViewModel by activityViewModels()
    @SuppressLint("SuspiciousIndentation")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                ContactsAppTheme {
                val contactState by mainViewModel.contactState.collectAsState()
                        
                    Column(
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Text(text = contactState.contacts.size.toString())
                        Text(text = "This is contact page")
                        Button(onClick = { findNavController().navigate(R.id.create_screen) }) {
                            Text(text = "New Contact")
                        }
                        ShowContactItems(
                            contacts = contactState.contacts,
                            navigate = { findNavController().navigate(R.id.detail_screen) },
                            viewModel = mainViewModel
                        )
                    }


                }
                }
            }
        }
    }


@Composable
fun ShowContactItems(contacts: List<Contact>, navigate: () -> Unit, viewModel: MainViewModel){
    LazyColumn(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        items(contacts) { contact ->
            Button(onClick = {
                viewModel.selectContact(contact)
                navigate() }, colors = ButtonDefaults.buttonColors(containerColor = Color.DarkGray),
                modifier = Modifier.defaultMinSize(minWidth = 300.dp, minHeight = 50.dp),
                contentPadding = PaddingValues(end = 190.dp),
            ) {
                Row(horizontalArrangement = Arrangement.Start, verticalAlignment = Alignment.CenterVertically) {
                Box(modifier = Modifier
                    .background(Color.Gray, shape = CircleShape)
                    .border(width = 2.dp, color = Color.Green, shape = CircleShape)
                    .layout() { measurable, constraints ->
                        // Measure the composable
                        val placeable = measurable.measure(constraints)
                        //get the current max dimension to assign width=height
                        val currentHeight = placeable.height
                        val currentWidth = placeable.width
                        val newDiameter = maxOf(currentHeight, currentWidth)

                        //assign the dimension and the center position
                        layout(newDiameter, newDiameter) {
                            // Where the composable gets placed
                            placeable.placeRelative(
                                (newDiameter - currentWidth) / 2,
                                (newDiameter - currentHeight) / 2
                            )
                        }
                    }) {
                    Text(
                        text = "${contact.firstName[0]}${contact.lastName[0]}",
                        textAlign = TextAlign.Center,
                        color = Color.White,
                        modifier = Modifier.padding(4.dp),
                    )
                }

            Text(text = "${contact.firstName} ${contact.lastName}", Modifier.padding(start = 15.dp))
                }
            }

        }
    }
}