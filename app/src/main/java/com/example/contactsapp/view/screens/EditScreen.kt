package com.example.contactsapp.view.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.contactsapp.R
import com.example.contactsapp.model.local.Address
import com.example.contactsapp.model.local.Contact
import com.example.contactsapp.ui.theme.ContactsAppTheme
import com.example.contactsapp.viewmodel.MainViewModel

class EditScreen : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val mainViewModel: MainViewModel by activityViewModels()
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val contactState = mainViewModel.contactState.collectAsState().value
                ContactsAppTheme {
                    ShowEditItems(
                        navigate = { findNavController().navigateUp() },
                        navigateHome = {findNavController().navigate(R.id.main_screen)},
                        contact = contactState.selectedContact,
                        editContact = {con-> mainViewModel.editContact(con)})
                }
            }
        }
    }
}

@Composable
fun ShowEditItems( contact: Contact?, navigate: () -> Unit, editContact: (Contact)->Unit, navigateHome: () ->Unit) {

    val _contact = remember {
        mutableStateOf(contact)
    }
    val _address = remember {
        mutableStateOf(Address())
    }
    val emailCount = remember {
        mutableStateOf(0)
    }
    val phoneCount = remember {
        mutableStateOf(0)
    }
    Column(
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.padding(top = 20.dp)
    ) {
        Text(text = "This is the edit page", Modifier.padding(20.dp))
        _contact.value?.let {
            TextFieldEdit(value = it.firstName, name = "First Name -Required", current = contact?.firstName) {
                _contact.value = _contact.value!!.copy(firstName = it)
            }
        }
        _contact.value?.let {
            TextFieldEdit(value = it.lastName, name = "Last Name -Required", current = contact?.lastName) {
                _contact.value = _contact.value!!.copy(lastName = it)
            }
        }
        _contact.value?.let {
            TextFieldEdit(value = it.singleEmail, name = "email", current = contact?.singleEmail) {
                _contact.value = _contact.value!!.copy(singleEmail = it)
            }
        }
        Row() {
            Button(onClick = { emailCount.value = emailCount.value+1
                _contact.value = _contact.value?.copy(email =
                _contact.value!!.email + _contact.value!!.singleEmail)
            }) {
                Text(text = "Add email")
            }
            Text(text = "${emailCount.value} emails added")
        }
        _contact.value?.let {
            TextFieldEdit(value = it.singlePhone, name = "phone", current = contact?.singlePhone) {
                _contact.value = _contact.value!!.copy(singlePhone = it)
            }
        }
        Row() {
            Button(onClick = { phoneCount.value = phoneCount.value+1
                _contact.value = _contact.value?.copy(phone =
                _contact.value!!.phone + _contact.value!!.singlePhone)
            }) {
                Text(text = "Add phone")
            }
            Text(text = "${phoneCount.value} phone numbers added")
        }

        _contact.value?.address?.let {
            TextFieldEdit(value = it.streetAddress, name = "Street Address", current = contact?.address?.streetAddress) {
                _address.value = _address.value.copy(streetAddress = it)
                _contact.value = _contact.value?.copy(address = _address.value)
            }
        }
        _contact.value?.address?.let {
            TextFieldEdit(value = it.city, name = "City", current = contact?.address?.city) {
                _address.value = _address.value.copy(city = it)
                _contact.value = _contact.value?.copy(address = _address.value)
            }
        }
        _contact.value?.address?.let {
            TextFieldEdit(value = it.state, name = "State", current = contact?.address?.state) {
                _address.value = _address.value.copy(state = it)
                _contact.value = _contact.value?.copy(address = _address.value)
            }
        }
        _contact.value?.address?.let {
            TextFieldEdit(value = it.zipcode, name = "Zipcode", current = contact?.address?.zipcode) {
                _address.value = _address.value.copy(zipcode = it)
                _contact.value = _contact.value?.copy(address = _address.value)
            }
        }

        Button(onClick = {
            if (_contact.value?.firstName!!.isNotEmpty()  && _contact.value!!.lastName.isNotEmpty()) {
                editContact(_contact.value!!)
                navigateHome()
            }
        }) {
            Text(text = "Save Contact")
        }
        Button(onClick = { navigate() }) {
            Text(text = "Go Back")
        }
    }
}
@Composable
fun TextFieldEdit(value: String, name:String, current:String?, onTextChange: (newText: String) -> Unit) {
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(text = "$name: ${current}")
        BasicTextField(
            value = value,
            onValueChange = { onTextChange(it) },
            Modifier.background(color = Color.LightGray)
        )
    }
}