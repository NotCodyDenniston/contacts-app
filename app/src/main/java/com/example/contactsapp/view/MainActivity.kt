package com.example.contactsapp.view

import androidx.fragment.app.FragmentActivity
import com.example.contactsapp.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : FragmentActivity(R.layout.activity_main)