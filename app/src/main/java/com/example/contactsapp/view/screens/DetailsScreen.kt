package com.example.contactsapp.view.screens

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.example.contactsapp.R
import com.example.contactsapp.model.local.Contact
import com.example.contactsapp.ui.theme.ContactsAppTheme
import com.example.contactsapp.utils.Resource
import com.example.contactsapp.viewmodel.MainViewModel

class DetailsScreen : Fragment() {
    val mainViewModel: MainViewModel by activityViewModels()
    @SuppressLint("SuspiciousIndentation")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                ContactsAppTheme {
                  val contactState = mainViewModel.contactState.collectAsState().value
                    ShowDetailItems(
                        contactState.selectedContact,
                        navigateToEdit = { findNavController().navigate(R.id.edit_screen) },
                        navigateToMain = { findNavController().navigate(R.id.main_screen)})
                }
            }
        }
    }
}

@Composable
fun ShowDetailItems(contact:Contact?, navigateToEdit: () -> Unit, navigateToMain: () -> Unit){
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(text = "This is the detail page!")
        Text(text ="${contact?.firstName} ${contact?.lastName}")
        Text(text ="Email: ${contact?.email}")
        Text(text ="Phone: ${contact?.phone}")
        Text(text ="Street: ${contact?.address?.streetAddress}")
        Text(text ="City/State: ${contact?.address?.city}, ${contact?.address?.state}")
        Text(text ="Zip: ${contact?.address?.zipcode}")
        Button(onClick = { navigateToEdit() }) {
            Text(text = "Edit Contact")
        }
        Button(onClick = { navigateToMain() }) {
            Text(text = "Go Back")
        }
    }
}