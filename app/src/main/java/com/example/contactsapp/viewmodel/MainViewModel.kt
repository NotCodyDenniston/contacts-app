package com.example.contactsapp.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import com.example.contactsapp.model.local.Contact
import com.example.contactsapp.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

@HiltViewModel
class MainViewModel @Inject constructor() : ViewModel() {

    private val _contacts: MutableStateFlow<ContactState> =
        MutableStateFlow(ContactState())
    val contactState: StateFlow<ContactState> get() = _contacts


    fun selectContact(selectedcontact: Contact) {
        with(_contacts) {
            value = value.copy(selectedContact = selectedcontact)
        }

    }

    fun editContact(editedContact: Contact){
       val newContacts = _contacts.value.contacts.filter { contact ->
          contact != _contacts.value.selectedContact
       }
        _contacts.value = _contacts.value.copy(contacts = newContacts + editedContact)
    }

    fun saveContact(contact: Contact) {
        //Actually go down to the repo and save your data (Add DB)
        _contacts.value = _contacts.value.copy(contacts = _contacts.value.contacts + contact)
        Log.e("%%%%%%%%%%","${_contacts.value.contacts}")

    }

}


