package com.example.contactsapp.viewmodel

import com.example.contactsapp.model.local.Contact

data class ContactState(
    val isLoading: Boolean = false,
    var contacts: List<Contact> = emptyList(),
    var selectedContact: Contact? = null,
    val errorMessage: String = ""
)