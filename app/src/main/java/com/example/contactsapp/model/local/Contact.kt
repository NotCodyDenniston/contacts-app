package com.example.contactsapp.model.local

import kotlinx.serialization.Serializable

@Serializable
data class Contact(
    val firstName: String = "",
    val lastName: String= "",
    val address: Address = Address(),
    val phone: List<String> = emptyList(),
    val email: List<String> = emptyList(),
    val singlePhone: String = "",
    val singleEmail: String = ""
)
