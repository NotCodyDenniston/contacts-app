package com.example.contactsapp.model.local

import kotlinx.serialization.Serializable

@Serializable
data class Address(
    val streetAddress: String = "",
    val city: String = "",
    val state: String = "",
    val zipcode: String = ""
)
